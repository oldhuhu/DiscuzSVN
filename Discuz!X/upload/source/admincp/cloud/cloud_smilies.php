<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id$
 */
if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

cpheader();

$_GET['anchor'] = in_array($_GET['anchor'], array('base')) ? $_GET['anchor'] : 'base';

shownav('navcloud', 'cloud_smilies');

showsubmenu('cloud_smilies');
showtips('cloud_smilies_tips');